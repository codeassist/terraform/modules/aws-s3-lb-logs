output "s3_lb_logs_bucket_id" {
  description = "The name of the bucket."
  value       = join("", aws_s3_bucket.logs.*.id)
}

output "s3_lb_logs_bucket_arn" {
  description = "The ARN of the bucket."
  value       = join("", aws_s3_bucket.logs.*.arn)
}

output "s3_lb_logs_bucket_domain_name" {
  description = "The bucket domain name (FQDN)."
  value       = join("", aws_s3_bucket.logs.*.bucket_domain_name)
}

output "s3_lb_logs_bucket_region" {
  description = "The AWS region this bucket resides in."
  value       = join("", aws_s3_bucket.logs.*.region)
}
