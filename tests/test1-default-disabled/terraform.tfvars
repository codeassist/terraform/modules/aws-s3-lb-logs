# --------------------------
# General/Common parameters
# --------------------------
# Whether to create the resources ("false" prevents the module from creating any resources).
s3_lb_logs_module_enabled = false

# ---------------------
# S3 bucket parameters
# ---------------------
# (Required) Creates a unique bucket name beginning with the specified prefix.
s3_lb_logs_bucket_prefix = "test-1"
# A mapping of tags to assign to the bucket.
s3_lb_logs_bucket_tags = {}
# A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed
# without error. These objects are not recoverable.
s3_lb_logs_bucket_force_destroy = false

# A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket.
s3_lb_logs_bucket_versioning_enabled = false

# The server-side encryption algorithm to use.
s3_lb_logs_bucket_sse_algorithm = "AES256"
# Enable lifecycle events on this bucket.
s3_lb_logs_bucket_lifecycle_rule_enabled = true
# Specifies when noncurrent object versions expire.
s3_lb_logs_bucket_noncurrent_version_expiration_days = 90
# Specifies when noncurrent object versions transitions.
s3_lb_logs_bucket_noncurrent_version_transition_days = 30
# Number of days to persist in the standard storage tier before moving to the infrequent access tier.
s3_lb_logs_bucket_standard_transition_days = 30
# Number of days after which to move the data to the glacier storage tier.
s3_lb_logs_bucket_glacier_transition_days = 60
# Number of days after which to expunge the objects.
s3_lb_logs_bucket_expiration_days = 90
