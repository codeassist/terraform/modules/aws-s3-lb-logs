# --------------------------
# General/Common parameters
# --------------------------
variable "s3_lb_logs_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

# ---------------------
# S3 bucket parameters
# ---------------------
variable "s3_lb_logs_bucket_prefix" {
  description = "(Required) Creates a unique bucket name beginning with the specified prefix. Conflicts with `bucket`."
  type        = string
}
variable "s3_lb_logs_bucket_tags" {
  description = "A mapping of tags to assign to the bucket."
  type        = map(string)
  default     = {}
}

variable "s3_lb_logs_bucket_force_destroy" {
  description = "A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable."
  type        = bool
  default     = false
}

variable "s3_lb_logs_bucket_versioning_enabled" {
  description = "A state of versioning. Versioning is a means of keeping multiple variants of an object in the same bucket."
  type        = bool
  default     = false
}

variable "s3_lb_logs_bucket_sse_algorithm" {
  description = "The server-side encryption algorithm to use."
  # Valid values are:
  #   * AES256
  #   * aws:kms
  type    = string
  default = "AES256"
}
variable "s3_lb_logs_bucket_kms_master_key_arn" {
  description = "The AWS KMS master key ARN used for the SSE-KMS encryption."
  # This can only be used when you set the value of `sse_algorithm` as `aws:kms`.
  # The default aws/s3 AWS KMS master key is used if this element is absent while the `sse_algorithm` is `aws:kms`.
  type    = string
  default = ""
}

variable "s3_lb_logs_bucket_lifecycle_rule_enabled" {
  description = "Enable lifecycle events on this bucket."
  type        = bool
  default     = true
}
variable "s3_lb_logs_bucket_noncurrent_version_expiration_days" {
  description = "Specifies when noncurrent object versions expire."
  default     = 90
}
variable "s3_lb_logs_bucket_noncurrent_version_transition_days" {
  description = "Specifies when noncurrent object versions transitions."
  default     = 30
}

variable "s3_lb_logs_bucket_standard_transition_days" {
  description = "Number of days to persist in the standard storage tier before moving to the infrequent access tier."
  default     = 30
}
variable "s3_lb_logs_bucket_glacier_transition_days" {
  description = "Number of days after which to move the data to the glacier storage tier."
  default     = 60
}
variable "s3_lb_logs_bucket_expiration_days" {
  description = "Number of days after which to expunge the objects."
  default     = 90
}
