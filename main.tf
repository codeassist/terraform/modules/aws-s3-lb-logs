locals {
  enabled = var.s3_lb_logs_module_enabled ? true : false

  tags = merge(
    var.s3_lb_logs_bucket_tags,
    {
      terraform = "true"
    },
  )
}

resource "random_id" "lifecycle" {
  # The resource `random_id` generates random numbers that are intended to be used as unique identifiers for other
  # resources.
  count = (local.enabled && var.s3_lb_logs_bucket_lifecycle_rule_enabled) ? 1 : 0

  # (Required) The number of random bytes to produce. The minimum value is 1, which produces eight bits of randomness.
  byte_length = 8
  # (Optional) Arbitrary map of values that, when changed, will trigger a new id to be generated. See the main provider
  # documentation for more information:
  #   * https://www.terraform.io/docs/providers/random/index.html
  keepers = {
    bucket = var.s3_lb_logs_bucket_prefix
  }
}

resource "aws_s3_bucket" "logs" {
  # Provides a S3 bucket resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique bucket name beginning with the specified prefix.
  # Conflicts with `bucket`.
  bucket_prefix = var.s3_lb_logs_bucket_prefix
  # (Optional) The canned ACL to apply.
  acl = "log-delivery-write"

  # (Optional) A mapping of tags to assign to the bucket.
  tags = merge(
    local.tags,
    {
      Name = var.s3_lb_logs_bucket_prefix
    },
  )

  # (Optional) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be
  # destroyed without error. These objects are not recoverable.
  force_destroy = var.s3_lb_logs_bucket_force_destroy

  # A state of versioning object:
  versioning {
    # (Optional) Enable versioning. Once you version-enable a bucket, it can never return to an unversioned state.
    # You can, however, suspend versioning on that bucket.
    enabled = var.s3_lb_logs_bucket_versioning_enabled
  }

  # (Optional) A configuration of server-side encryption, see more at:
  #   * https://docs.aws.amazon.com/AmazonS3/latest/dev/bucket-encryption.html
  #   * https://www.terraform.io/docs/providers/aws/r/s3_bucket.html#enable-default-server-side-encryption
  server_side_encryption_configuration {
    # (required) A single object for server-side encryption by default configuration.
    rule {
      # (required) A single object for setting server-side encryption by default.
      apply_server_side_encryption_by_default {
        # (required) The server-side encryption algorithm to use. Valid values are `AES256` and `aws:kms`
        sse_algorithm = var.s3_lb_logs_bucket_sse_algorithm

        # (optional) The AWS KMS master key ID used for the SSE-KMS encryption. This can only be used when you set
        # the value of `sse_algorithm` as `aws:kms`. The default `aws/s3` AWS KMS master key is used if this element
        # is absent while the `sse_algorithm` is `aws:kms`.
        kms_master_key_id = var.s3_lb_logs_bucket_kms_master_key_arn
      }
    }
  }

  # (Optional) A configuration of object lifecycle management, see at:
  #   * https://docs.aws.amazon.com/en_us/AmazonS3/latest/dev/object-lifecycle-mgmt.html
  lifecycle_rule {
    # (Required) Specifies lifecycle rule status.
    enabled = var.s3_lb_logs_bucket_lifecycle_rule_enabled

    # (Optional) Unique identifier for the rule.
    id = join("", random_id.lifecycle.*.hex)
    # (Optional) Specifies object tags key and value.
    tags = local.tags

    # At least one of must be specified:
    #   * expiration,
    #   * transition,
    #   * noncurrent_version_expiration,
    #   * noncurrent_version_transition

    # (Optional) Specifies when noncurrent object versions expire.
    noncurrent_version_expiration {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days = var.s3_lb_logs_bucket_noncurrent_version_expiration_days
    }

    # (Optional) Specifies when noncurrent object versions transitions.
    noncurrent_version_transition {
      # (Optional) Specifies the number of days after object creation when the specific rule action takes effect.
      days = var.s3_lb_logs_bucket_noncurrent_version_transition_days
      # (Required) Specifies the Amazon S3 storage class to which you want the object to transition. Can be:
      #   * ONEZONE_IA,
      #   * STANDARD_IA,
      #   * INTELLIGENT_TIERING,
      #   * GLACIER,
      #   * DEEP_ARCHIVE.
      storage_class = "GLACIER"
    }

    # Any `expiration` and/or `transition` objects supports the same arguments (see above).

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.s3_lb_logs_bucket_standard_transition_days
      storage_class = "STANDARD_IA"
    }

    # (Optional) Specifies a period in the object's transitions.
    transition {
      days          = var.s3_lb_logs_bucket_glacier_transition_days
      storage_class = "GLACIER"
    }

    # (Optional) Specifies a period in the object's expire.
    expiration {
      days = var.s3_lb_logs_bucket_expiration_days
    }
  }
}

data "aws_elb_service_account" "this" {
  # Use this data source to get the Account ID of the AWS Elastic Load Balancing Service Account in a given region for
  # the purpose of whitelisting in S3 bucket policy:
  #   * http://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html#attach-bucket-policy
  count = local.enabled ? 1 : 0
}
data "aws_iam_policy_document" "elb" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the `aws_iam_policy` resource.
  count = local.enabled ? 1 : 0
  # (Optional) - An ID for the policy document.
  policy_id = "${var.s3_lb_logs_bucket_prefix}ELBAccessLogs"

  statement {
    sid    = "AllowELBAccountWriteLogsIntoTheBucket"
    effect = "Allow"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "${join("", aws_s3_bucket.logs.*.arn)}/AWSLogs/*",
    ]
    principals {
      type = "AWS"
      identifiers = [
        join("", data.aws_elb_service_account.this.*.arn)
      ]
    }
  }

  statement {
    sid    = "AWSLogDeliveryWrite"
    effect = "Allow"
    actions = [
      "s3:PutObject",
    ]
    resources = [
      "${join("", aws_s3_bucket.logs.*.arn)}/AWSLogs/*",
    ]
    principals {
      type = "Service"
      identifiers = [
        "delivery.logs.amazonaws.com"
      ]
    }
    condition {
      test     = "StringEquals"
      variable = "s3:x-amz-acl"
      values = [
        "bucket-owner-full-control",
      ]
    }
  }

  statement {
    sid    = "AWSLogDeliveryAclCheck"
    effect = "Allow"
    actions = [
      "s3:GetBucketAcl",
    ]
    resources = [
      join("", aws_s3_bucket.logs.*.arn),
    ]
    principals {
      type = "Service"
      identifiers = [
        "delivery.logs.amazonaws.com"
      ]
    }
  }
}
resource "aws_s3_bucket_policy" "logs" {
  # Attaches a policy to an S3 bucket resource.
  count = local.enabled ? 1 : 0

  # (Required) The name of the bucket to which to apply the policy.
  bucket = join("", aws_s3_bucket.logs.*.id)
  # (Required) The text of the policy.
  policy = join("", data.aws_iam_policy_document.elb.*.json)
}
